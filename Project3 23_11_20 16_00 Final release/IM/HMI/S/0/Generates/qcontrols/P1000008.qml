﻿import QtQuick 2.7
import "qrc:/"
IGuiPage
{
	id: q16777224
	objId: 16777224
	x: 0
	y: 0
	width: 480
	height: 272
	IGuiIOField
	{
		id: q33554467
		objId: 33554467
		x: 336
		y: 136
		width: 48
		height: 24
		qm_ImageSource: "image://QSmartImageProvider/50#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ff313031"
		qm_TextColor: "#ff9ccfff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiIOField
	{
		id: q33554443
		objId: 33554443
		x: 268
		y: 64
		width: 212
		height: 28
		qm_ImageSource: "image://QSmartImageProvider/54#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ff313031"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiTextField
	{
		id: q268435487
		objId: 268435487
		x: 364
		y: 28
		width: 75
		height: 36
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiQmlCircle
	{
		id: q671088656
		objId: 671088656
		x: 440
		y: 32
		width: 29
		height: 29
		qm_BorderWidth: 1
		qm_TextColor: "#ff181c31"
		qm_FillColor: "#ffc6c3c6"
		qm_Radius : 14
		qm_EllipseWidth: 29
		qm_EllipseHeight: 29
	}
	IGuiSwitch
	{
		id: q352321548
		objId: 352321548
		x: 12
		y: 92
		width: 108
		height: 48
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_RectangleBorder.width:2
		qm_RectangleBorder.color:"#ff424952"
		qm_FillColor: "#ff848284"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 2
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiSwitch
	{
		id: q352321549
		objId: 352321549
		x: 128
		y: 92
		width: 108
		height: 48
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_RectangleBorder.width:2
		qm_RectangleBorder.color:"#ff424952"
		qm_FillColor: "#ff848284"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 2
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiTextField
	{
		id: q268435488
		objId: 268435488
		x: 12
		y: 64
		width: 107
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiTextField
	{
		id: q268435489
		objId: 268435489
		x: 156
		y: 64
		width: 51
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiSwitch
	{
		id: q352321550
		objId: 352321550
		x: 388
		y: 228
		width: 64
		height: 44
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/42#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 2
		qm_Border.left: 2
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 1
		qm_Anchors.leftMargin: 1
		qm_Anchors.rightMargin: 1
		qm_Anchors.topMargin: 1
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiLine
	{
		id: q671088657
		objId: 671088657
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff9c9aa5"
		qm_LineStartArrow: false
		qm_LineEndArrow: false
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: true
	}
	IGuiTextField
	{
		id: q268435490
		objId: 268435490
		x: 296
		y: 4
		width: 152
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiLine
	{
		id: q671088658
		objId: 671088658
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff9c9aa5"
		qm_LineStartArrow: false
		qm_LineEndArrow: false
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: true
	}
	IGuiLine
	{
		id: q671088659
		objId: 671088659
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff9c9aa5"
		qm_LineStartArrow: false
		qm_LineEndArrow: false
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: true
	}
	IGuiIOField
	{
		id: q33554444
		objId: 33554444
		x: 272
		y: 108
		width: 92
		height: 24
		qm_ImageSource: "image://QSmartImageProvider/49#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ffffffff"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiSwitch
	{
		id: q352321551
		objId: 352321551
		x: 12
		y: 8
		width: 108
		height: 48
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_RectangleBorder.width:2
		qm_RectangleBorder.color:"#ff424952"
		qm_FillColor: "#ff848284"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 2
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiIOField
	{
		id: q33554445
		objId: 33554445
		x: 376
		y: 188
		width: 92
		height: 24
		qm_ImageSource: "image://QSmartImageProvider/49#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ffffffff"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiContainer
	{
		id: q369098753
		objId: 369098753
		x: 312
		y: 32
		width: 29
		height: 29
		IGuiQmlCircle
		{
			id: q671088661
			objId: 671088661
			x: 0
			y: 0
			width: 29
			height: 29
			qm_BorderWidth: 1
			qm_TextColor: "#ff181c31"
			qm_FillColor: "#ffc6c3c6"
			qm_Radius : 14
			qm_EllipseWidth: 29
			qm_EllipseHeight: 29
		}
		IGuiTextField
		{
			id: q268435492
			objId: 268435492
			x: 4
			y: 4
			width: 21
			height: 20
			qm_Transparent : true 
			qm_TextColor: "#ffffffff"
			qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
			qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
			qm_Anchors.bottomMargin: 2
			qm_Anchors.leftMargin: 3
			qm_Anchors.rightMargin: 2
			qm_Anchors.topMargin: 2
		}
	}
	IGuiContainer
	{
		id: q369098752
		objId: 369098752
		x: 276
		y: 32
		width: 29
		height: 29
		IGuiQmlCircle
		{
			id: q671088660
			objId: 671088660
			x: 0
			y: 0
			width: 29
			height: 29
			qm_BorderWidth: 1
			qm_TextColor: "#ff181c31"
			qm_FillColor: "#ffc6c3c6"
			qm_Radius : 14
			qm_EllipseWidth: 29
			qm_EllipseHeight: 29
		}
		IGuiTextField
		{
			id: q268435491
			objId: 268435491
			x: 4
			y: 4
			width: 21
			height: 20
			qm_Transparent : true 
			qm_TextColor: "#ffffffff"
			qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
			qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
			qm_Anchors.bottomMargin: 2
			qm_Anchors.leftMargin: 3
			qm_Anchors.rightMargin: 2
			qm_Anchors.topMargin: 2
		}
	}
	IGuiIOField
	{
		id: q33554446
		objId: 33554446
		x: 376
		y: 108
		width: 92
		height: 24
		qm_ImageSource: "image://QSmartImageProvider/49#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ffffffff"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiTextField
	{
		id: q268435493
		objId: 268435493
		x: 152
		y: 24
		width: 59
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ff9ccfff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiTextField
	{
		id: q268435494
		objId: 268435494
		x: 272
		y: 92
		width: 88
		height: 15
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiTextField
	{
		id: q268435495
		objId: 268435495
		x: 380
		y: 92
		width: 81
		height: 15
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiTextField
	{
		id: q268435496
		objId: 268435496
		x: 268
		y: 140
		width: 74
		height: 17
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiTextField
	{
		id: q268435497
		objId: 268435497
		x: 368
		y: 172
		width: 108
		height: 15
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiSwitch
	{
		id: q352321553
		objId: 352321553
		x: 12
		y: 176
		width: 108
		height: 44
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_RectangleBorder.width:2
		qm_RectangleBorder.color:"#ff424952"
		qm_FillColor: "#ff848284"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 2
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiTextField
	{
		id: q268435498
		objId: 268435498
		x: 144
		y: 148
		width: 79
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiLine
	{
		id: q671088662
		objId: 671088662
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff9c9aa5"
		qm_LineStartArrow: false
		qm_LineEndArrow: false
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: true
	}
	IGuiIOField
	{
		id: q33554448
		objId: 33554448
		x: 428
		y: 136
		width: 48
		height: 24
		qm_ImageSource: "image://QSmartImageProvider/50#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ff313031"
		qm_TextColor: "#ff9ccfff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiIOField
	{
		id: q33554447
		objId: 33554447
		x: 372
		y: 136
		width: 64
		height: 24
		qm_ImageSource: "image://QSmartImageProvider/50#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ff313031"
		qm_TextColor: "#ff9ccfff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiSwitch
	{
		id: q352321552
		objId: 352321552
		x: 28
		y: 228
		width: 64
		height: 44
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/42#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 2
		qm_Border.left: 2
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 1
		qm_Anchors.leftMargin: 1
		qm_Anchors.rightMargin: 1
		qm_Anchors.topMargin: 1
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiTextField
	{
		id: q268435499
		objId: 268435499
		x: 144
		y: 24
		width: 76
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ff9ccfff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiTextField
	{
		id: q268435500
		objId: 268435500
		x: 132
		y: 16
		width: 108
		height: 36
		qm_Transparent : true 
		qm_TextColor: "#ff9ccfff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiTextField
	{
		id: q268435501
		objId: 268435501
		x: 144
		y: 24
		width: 82
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ff9ccfff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiIOField
	{
		id: q33554449
		objId: 33554449
		x: 148
		y: 176
		width: 184
		height: 44
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 4
		qm_ImageSource: "image://QSmartImageProvider/44#2#4#128#0#0"
		qm_Border.top: 5
		qm_Border.bottom: 5
		qm_Border.right: 5
		qm_Border.left: 5
		qm_FillColor: "#ffffffff"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff000000"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 6
		qm_Anchors.leftMargin: 7
		qm_Anchors.rightMargin: 6
		qm_Anchors.topMargin: 6
	}
	IGuiGraphicSwitch
	{
		id: q352321554
		objId: 352321554
		x: 268
		y: 228
		width: 64
		height: 44
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/46#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 2
		qm_Border.left: 2
		qm_FillColor: "#ffe7e3e7"
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_ImagePossitionX: 1
		qm_ImagePossitionY: 1
		qm_ImageWidth: 62
		qm_ImageHeight: 42
		qm_SourceSizeWidth: 62
		qm_SourceSizeHeight: 42
	}
	IGuiGraphicSwitch
	{
		id: q352321555
		objId: 352321555
		x: 148
		y: 228
		width: 64
		height: 44
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/46#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 2
		qm_Border.left: 2
		qm_FillColor: "#ffe7e3e7"
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_ImagePossitionX: 1
		qm_ImagePossitionY: 1
		qm_ImageWidth: 62
		qm_ImageHeight: 42
		qm_SourceSizeWidth: 62
		qm_SourceSizeHeight: 42
	}
	IGuiTextField
	{
		id: q268435502
		objId: 268435502
		x: 144
		y: 24
		width: 88
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ff9ccf00"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiGraphicButton
	{
		id: q486539314
		objId: 486539314
		x: 124
		y: 8
		width: 120
		height: 48
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/57#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_Transparent : true 
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_ImagePossitionX: 1
		qm_ImagePossitionY: 1
		qm_ImageWidth: 118
		qm_ImageHeight: 46
		qm_SourceSizeWidth: 118
		qm_SourceSizeHeight: 46
	}
	IGuiQmlCircle
	{
		id: q671088664
		objId: 671088664
		x: 344
		y: 32
		width: 12
		height: 12
		qm_BorderWidth: 1
		qm_TextColor: "#ff9c9aa5"
		qm_FillColor: "#ffffcf00"
		qm_Radius : 6
		qm_EllipseWidth: 12
		qm_EllipseHeight: 12
	}
}
