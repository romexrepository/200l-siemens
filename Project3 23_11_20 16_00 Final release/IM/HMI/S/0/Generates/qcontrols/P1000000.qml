﻿import QtQuick 2.7
import "qrc:/"
IGuiPage
{
	id: q16777216
	objId: 16777216
	x: 0
	y: 0
	width: 480
	height: 272
	IGuiSwitch
	{
		id: q352321536
		objId: 352321536
		x: 12
		y: 176
		width: 108
		height: 44
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_RectangleBorder.width:2
		qm_RectangleBorder.color:"#ff424952"
		qm_FillColor: "#ff848284"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 2
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiTextField
	{
		id: q268435480
		objId: 268435480
		x: 360
		y: 28
		width: 75
		height: 36
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiQmlCircle
	{
		id: q671088649
		objId: 671088649
		x: 440
		y: 32
		width: 29
		height: 29
		qm_BorderWidth: 1
		qm_TextColor: "#ff181c31"
		qm_FillColor: "#ffc6c3c6"
		qm_Radius : 14
		qm_EllipseWidth: 29
		qm_EllipseHeight: 29
	}
	IGuiSwitch
	{
		id: q352321537
		objId: 352321537
		x: 368
		y: 176
		width: 48
		height: 48
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_RectangleBorder.width:2
		qm_RectangleBorder.color:"#ff424952"
		qm_FillColor: "#ff848284"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 2
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiSwitch
	{
		id: q352321538
		objId: 352321538
		x: 420
		y: 176
		width: 48
		height: 48
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_RectangleBorder.width:2
		qm_RectangleBorder.color:"#ff424952"
		qm_FillColor: "#ff848284"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 2
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiSwitch
	{
		id: q352321539
		objId: 352321539
		x: 388
		y: 228
		width: 64
		height: 44
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/42#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 2
		qm_Border.left: 2
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 1
		qm_Anchors.leftMargin: 1
		qm_Anchors.rightMargin: 1
		qm_Anchors.topMargin: 1
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiSwitch
	{
		id: q352321540
		objId: 352321540
		x: 28
		y: 228
		width: 64
		height: 44
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/42#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 2
		qm_Border.left: 2
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 1
		qm_Anchors.leftMargin: 1
		qm_Anchors.rightMargin: 1
		qm_Anchors.topMargin: 1
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiTextField
	{
		id: q268435481
		objId: 268435481
		x: 388
		y: 4
		width: 59
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiLine
	{
		id: q671088651
		objId: 671088651
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff9c9aa5"
		qm_LineStartArrow: false
		qm_LineEndArrow: false
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: true
	}
	IGuiSwitch
	{
		id: q352321541
		objId: 352321541
		x: 12
		y: 8
		width: 108
		height: 48
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_RectangleBorder.width:2
		qm_RectangleBorder.color:"#ff424952"
		qm_FillColor: "#ff848284"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 2
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiButton
	{
		id: q486539312
		objId: 486539312
		x: 128
		y: 8
		width: 108
		height: 48
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/19#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 5
		qm_Border.left: 5
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 1
		qm_Anchors.leftMargin: 1
		qm_Anchors.rightMargin: 1
		qm_Anchors.topMargin: 1
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiSwitch
	{
		id: q352321542
		objId: 352321542
		x: 12
		y: 92
		width: 108
		height: 48
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_RectangleBorder.width:2
		qm_RectangleBorder.color:"#ff424952"
		qm_FillColor: "#ff848284"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 2
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiSwitch
	{
		id: q352321543
		objId: 352321543
		x: 128
		y: 92
		width: 108
		height: 48
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_RectangleBorder.width:2
		qm_RectangleBorder.color:"#ff424952"
		qm_FillColor: "#ff848284"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 2
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiTextField
	{
		id: q268435482
		objId: 268435482
		x: 12
		y: 64
		width: 107
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiTextField
	{
		id: q268435483
		objId: 268435483
		x: 156
		y: 64
		width: 51
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiLine
	{
		id: q671088652
		objId: 671088652
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff9c9aa5"
		qm_LineStartArrow: false
		qm_LineEndArrow: false
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: true
	}
	IGuiLine
	{
		id: q671088653
		objId: 671088653
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff9c9aa5"
		qm_LineStartArrow: false
		qm_LineEndArrow: false
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: true
	}
	IGuiIOField
	{
		id: q33554437
		objId: 33554437
		x: 148
		y: 176
		width: 184
		height: 44
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 4
		qm_ImageSource: "image://QSmartImageProvider/44#2#4#128#0#0"
		qm_Border.top: 5
		qm_Border.bottom: 5
		qm_Border.right: 5
		qm_Border.left: 5
		qm_FillColor: "#ffffffff"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff000000"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 6
		qm_Anchors.leftMargin: 7
		qm_Anchors.rightMargin: 6
		qm_Anchors.topMargin: 6
	}
	IGuiTextField
	{
		id: q268435484
		objId: 268435484
		x: 144
		y: 148
		width: 79
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiLine
	{
		id: q671088654
		objId: 671088654
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff9c9aa5"
		qm_LineStartArrow: false
		qm_LineEndArrow: false
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: true
	}
	IGuiGraphicSwitch
	{
		id: q352321544
		objId: 352321544
		x: 268
		y: 228
		width: 64
		height: 44
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/46#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 2
		qm_Border.left: 2
		qm_FillColor: "#ffe7e3e7"
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_ImagePossitionX: 1
		qm_ImagePossitionY: 1
		qm_ImageWidth: 62
		qm_ImageHeight: 42
		qm_SourceSizeWidth: 62
		qm_SourceSizeHeight: 42
	}
	IGuiGraphicSwitch
	{
		id: q352321545
		objId: 352321545
		x: 148
		y: 228
		width: 64
		height: 44
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/46#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 2
		qm_Border.left: 2
		qm_FillColor: "#ffe7e3e7"
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_ImagePossitionX: 1
		qm_ImagePossitionY: 1
		qm_ImageWidth: 62
		qm_ImageHeight: 42
		qm_SourceSizeWidth: 62
		qm_SourceSizeHeight: 42
	}
	IGuiTextField
	{
		id: q268435485
		objId: 268435485
		x: 272
		y: 4
		width: 50
		height: 20
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiLine
	{
		id: q671088655
		objId: 671088655
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff9c9aa5"
		qm_LineStartArrow: false
		qm_LineEndArrow: false
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: true
	}
	IGuiIOField
	{
		id: q33554438
		objId: 33554438
		x: 248
		y: 32
		width: 92
		height: 24
		qm_ImageSource: "image://QSmartImageProvider/49#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ffffffff"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiTextField
	{
		id: q268435486
		objId: 268435486
		x: 260
		y: 96
		width: 74
		height: 17
		qm_Transparent : true 
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiSwitch
	{
		id: q352321546
		objId: 352321546
		x: 248
		y: 60
		width: 92
		height: 32
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/42#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 2
		qm_Border.left: 2
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 1
		qm_Anchors.leftMargin: 1
		qm_Anchors.rightMargin: 1
		qm_Anchors.topMargin: 1
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiIOField
	{
		id: q33554441
		objId: 33554441
		x: 360
		y: 64
		width: 112
		height: 28
		qm_ImageSource: "image://QSmartImageProvider/54#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ff313031"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ffffffff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiIOField
	{
		id: q33554442
		objId: 33554442
		x: 368
		y: 136
		width: 100
		height: 32
		qm_ImageSource: "image://QSmartImageProvider/49#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ffffffff"
		qm_FocusWidth: 0
		qm_FocusColor: "#00000000"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiSliderSwitchHorizontal
	{
		id: q352321547
		objId: 352321547
		x: 368
		y: 96
		width: 100
		height: 32
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/55#2#4#128#0#0"
		qm_Border.top: 5
		qm_Border.bottom: 5
		qm_Border.right: 5
		qm_Border.left: 5
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 1
		qm_Anchors.leftMargin: 1
		qm_Anchors.rightMargin: 1
		qm_Anchors.topMargin: 1
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_SliderRectHeight: 31
		qm_SliderRectWidth: 49
		qm_SliderRectBorderCornerRadius: 3
		qm_SliderRectBorderWidth: 1
		qm_SliderRectImageID: 19
		qm_SliderRectTileTop: 15
		qm_SliderRectTileBottom: 15
		qm_SliderRectTileRight: 5
		qm_SliderRectTileLeft: 5
		qm_SliderGripBorderCornerRadius: 3
		qm_SliderGripBorderWidth: 1
		qm_SliderGripImageID: 56
		qm_SliderGripTileWidth: 8
		qm_SliderGripTileHeight: 56
		qm_SliderGripTileTop: 16
		qm_SliderGripTileBottom: 16
		qm_SliderGripTileRight: 0
		qm_SliderGripTileLeft: 0
		qm_SliderRectOffFillColor: "#fff7f3f7"
		qm_SliderRectOnFillColor: "#fff7f3f7"
		qm_SwichOnSide: false
		qm_SwitchOnValue: 1
	}
	IGuiIOField
	{
		id: q33554468
		objId: 33554468
		x: 236
		y: 112
		width: 28
		height: 24
		qm_ImageSource: "image://QSmartImageProvider/50#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ff313031"
		qm_TextColor: "#ff9ccfff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiIOField
	{
		id: q33554439
		objId: 33554439
		x: 312
		y: 112
		width: 48
		height: 24
		qm_ImageSource: "image://QSmartImageProvider/50#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ff313031"
		qm_TextColor: "#ff9ccfff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiIOField
	{
		id: q33554440
		objId: 33554440
		x: 260
		y: 112
		width: 60
		height: 24
		qm_ImageSource: "image://QSmartImageProvider/50#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_FillColor: "#ff313031"
		qm_TextColor: "#ff9ccfff"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 1
		qm_Anchors.topMargin: 2
	}
	IGuiLine
	{
		id: q671088650
		objId: 671088650
		qm_LineWidth: 1
		qm_BorderStyle: 0
		qm_TextColor: "#ff9c9aa5"
		qm_LineStartArrow: false
		qm_LineEndArrow: false
		qm_LineEndsShape: 1
		qm_FillColor: "#ffffffff"
		qm_FillStyle: true
	}
	IGuiGraphicView
	{
		id: q301989888
		objId: 301989888
		x: 104
		y: 0
		width: 276
		height: 272
		qm_Transparent : true 
		qm_ImageWidth: 276
		qm_ImageHeight: 272
		qm_SourceSizeWidth: 276
		qm_SourceSizeHeight: 272
	}
	IGuiGraphicButton
	{
		id: q486539313
		objId: 486539313
		x: 144
		y: 36
		width: 192
		height: 196
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 2
		qm_ImageSource: "image://QSmartImageProvider/53#2#4#128#0#0"
		qm_Border.top: 0
		qm_Border.bottom: 0
		qm_Border.right: 0
		qm_Border.left: 0
		qm_Transparent : true 
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_ImagePossitionX: 2
		qm_ImagePossitionY: 2
		qm_ImageWidth: 188
		qm_ImageHeight: 192
		qm_SourceSizeWidth: 188
		qm_SourceSizeHeight: 192
	}
}
