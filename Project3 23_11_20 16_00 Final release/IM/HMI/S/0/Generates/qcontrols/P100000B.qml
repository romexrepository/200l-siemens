﻿import QtQuick 2.7
import "qrc:/"
IGuiPage
{
	id: q16777227
	objId: 16777227
	x: 0
	y: 0
	width: 480
	height: 272
	IGuiGraphicSwitch
	{
		id: q352321560
		objId: 352321560
		x: 28
		y: 228
		width: 64
		height: 44
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/46#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 2
		qm_Border.left: 2
		qm_FillColor: "#ffe7e3e7"
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_ImagePossitionX: 1
		qm_ImagePossitionY: 1
		qm_ImageWidth: 62
		qm_ImageHeight: 42
		qm_SourceSizeWidth: 62
		qm_SourceSizeHeight: 42
	}
	IGuiTrendView
	{
		id: q469762048
		objId: 469762048
		x: 0
		y: 4
		width: 480
		height: 220
		qm_BorderCornerRadius: 4
		qm_BorderWidth: 1
		qm_RectangleBorder.color:"#ff636973"
		qm_FillColor: "#fff7f3f7"
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		rulerColor: "#31344a"
		rulerVisibility: false
		qm_TrendXPos: 2
		qm_TrendYPos: 2
		qm_TrendWidth: 476
		qm_TrendHeight: 182
		IGuiGraphicSwitch
		{
			id: q352321561
			objId: 352321561
			x: 7
			y: 184
			width: 44
			height: 32
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_ImageSource: "image://QSmartImageProvider/38#2#4#128#0#0"
			qm_Border.top: 15
			qm_Border.bottom: 15
			qm_Border.right: 5
			qm_Border.left: 5
			qm_FillColor: "#ffefebef"
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_ImageFillMode:6
			qm_ImagePossitionX: 4
			qm_ImagePossitionY: 2
			qm_ImageWidth: 38
			qm_ImageHeight: 28
			qm_SourceSizeWidth: 38
			qm_SourceSizeHeight: 28
		}
		IGuiGraphicButton
		{
			id: q486539331
			objId: 486539331
			x: 56
			y: 184
			width: 44
			height: 32
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_ImageSource: "image://QSmartImageProvider/38#2#4#128#0#0"
			qm_Border.top: 15
			qm_Border.bottom: 15
			qm_Border.right: 5
			qm_Border.left: 5
			qm_FillColor: "#ffefebef"
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_ImageFillMode:6
			qm_ImagePossitionX: 4
			qm_ImagePossitionY: 2
			qm_ImageWidth: 38
			qm_ImageHeight: 28
			qm_SourceSizeWidth: 38
			qm_SourceSizeHeight: 28
		}
		IGuiGraphicButton
		{
			id: q486539332
			objId: 486539332
			x: 105
			y: 184
			width: 44
			height: 32
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_ImageSource: "image://QSmartImageProvider/38#2#4#128#0#0"
			qm_Border.top: 15
			qm_Border.bottom: 15
			qm_Border.right: 5
			qm_Border.left: 5
			qm_FillColor: "#ffefebef"
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_ImageFillMode:6
			qm_ImagePossitionX: 4
			qm_ImagePossitionY: 2
			qm_ImageWidth: 38
			qm_ImageHeight: 28
			qm_SourceSizeWidth: 38
			qm_SourceSizeHeight: 28
		}
		IGuiGraphicButton
		{
			id: q486539333
			objId: 486539333
			x: 154
			y: 184
			width: 44
			height: 32
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_ImageSource: "image://QSmartImageProvider/38#2#4#128#0#0"
			qm_Border.top: 15
			qm_Border.bottom: 15
			qm_Border.right: 5
			qm_Border.left: 5
			qm_FillColor: "#ffefebef"
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_ImageFillMode:6
			qm_ImagePossitionX: 4
			qm_ImagePossitionY: 2
			qm_ImageWidth: 38
			qm_ImageHeight: 28
			qm_SourceSizeWidth: 38
			qm_SourceSizeHeight: 28
		}
		IGuiGraphicButton
		{
			id: q486539334
			objId: 486539334
			x: 203
			y: 184
			width: 44
			height: 32
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_ImageSource: "image://QSmartImageProvider/38#2#4#128#0#0"
			qm_Border.top: 15
			qm_Border.bottom: 15
			qm_Border.right: 5
			qm_Border.left: 5
			qm_FillColor: "#ffefebef"
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_ImageFillMode:6
			qm_ImagePossitionX: 4
			qm_ImagePossitionY: 2
			qm_ImageWidth: 38
			qm_ImageHeight: 28
			qm_SourceSizeWidth: 38
			qm_SourceSizeHeight: 28
		}
		IGuiGraphicButton
		{
			id: q486539335
			objId: 486539335
			x: 252
			y: 184
			width: 44
			height: 32
			qm_BorderCornerRadius: 3
			qm_BorderWidth: 1
			qm_ImageSource: "image://QSmartImageProvider/38#2#4#128#0#0"
			qm_Border.top: 15
			qm_Border.bottom: 15
			qm_Border.right: 5
			qm_Border.left: 5
			qm_FillColor: "#ffefebef"
			qm_FocusWidth: 2
			qm_FocusColor: "#ff94b6e7"
			qm_ImageFillMode:6
			qm_ImagePossitionX: 4
			qm_ImagePossitionY: 2
			qm_ImageWidth: 38
			qm_ImageHeight: 28
			qm_SourceSizeWidth: 38
			qm_SourceSizeHeight: 28
		}
	}
}
