﻿import QtQuick 2.7
import "qrc:/"
IGuiTemplate
{	id: q637534208
	objId: 637534208
	x: 0
	y: 0
	width: 480
	height: 272
	IGuiButton
	{
		id: q486539315
		objId: 486539315
		x: 388
		y: 227
		width: 63
		height: 44
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/19#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 5
		qm_Border.left: 5
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 1
		qm_Anchors.leftMargin: 1
		qm_Anchors.rightMargin: 1
		qm_Anchors.topMargin: 1
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiButton
	{
		id: q486539316
		objId: 486539316
		x: 268
		y: 227
		width: 63
		height: 44
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/19#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 5
		qm_Border.left: 5
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 1
		qm_Anchors.leftMargin: 1
		qm_Anchors.rightMargin: 1
		qm_Anchors.topMargin: 1
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiButton
	{
		id: q486539317
		objId: 486539317
		x: 148
		y: 227
		width: 63
		height: 44
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/19#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 5
		qm_Border.left: 5
		qm_FillColor: "#ffe7e3e7"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignHCenter
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 1
		qm_Anchors.leftMargin: 1
		qm_Anchors.rightMargin: 1
		qm_Anchors.topMargin: 1
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
	}
	IGuiGraphicButton
	{
		id: q486539318
		objId: 486539318
		x: 28
		y: 227
		width: 63
		height: 44
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_ImageSource: "image://QSmartImageProvider/61#2#4#128#0#0"
		qm_Border.top: 15
		qm_Border.bottom: 15
		qm_Border.right: 5
		qm_Border.left: 5
		qm_FillColor: "#ffe7e3e7"
		qm_FocusWidth: 2
		qm_FocusColor: "#ff94b6e7"
		qm_ImagePossitionX: 1
		qm_ImagePossitionY: 1
		qm_ImageWidth: 61
		qm_ImageHeight: 42
		qm_SourceSizeWidth: 61
		qm_SourceSizeHeight: 42
	}
	IGuiSymbolicIOField
	{
		id: q335544321
		objId: 335544321
		x: 82
		y: 8
		width: 311
		height: 29
		qm_BorderCornerRadius: 3
		qm_BorderWidth: 1
		qm_RectangleBorder.color:"#ff9c9aa5"
		qm_FillColor: "#ffffffff"
		qm_TextColor: "#ff31344a"
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 3
		qm_Anchors.leftMargin: 4
		qm_Anchors.rightMargin: 28
		qm_Anchors.topMargin: 3
		qm_FocusWidth: 0
		qm_FocusColor: "#ff000000"
		qm_GraphicImageId : 6 
		qm_IsDownButtonVisible: true
		qm_SymIOType: 1
		qm_NoOfVisibleRows:3
		qm_HotAreaWidth:25
		qm_SymIOSelBackFillColor: "#ffa5cfd6"
		qm_SymIOAltBackFillColor: "#ffe7e7ef"
		qm_SymIOSelForeColor:"#ffffffff"
		qm_NoOfSymIoTextListItems:7
		qm_hasScrollIndicator: false
	}
	IGuiIOField
	{
		id: q33554455
		objId: 33554455
		x: 395
		y: 22
		width: 85
		height: 22
		qm_Transparent : true 
		qm_TextColor: "#ff000000"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiIOField
	{
		id: q33554456
		objId: 33554456
		x: 395
		y: 0
		width: 85
		height: 22
		qm_Transparent : true 
		qm_TextColor: "#ff000000"
		qm_ValueVarTextAlignmentHorizontal: Text.AlignRight
		qm_ValueVarTextAlignmentVertical: Text.AlignVCenter
		qm_Anchors.bottomMargin: 2
		qm_Anchors.leftMargin: 3
		qm_Anchors.rightMargin: 2
		qm_Anchors.topMargin: 2
	}
	IGuiGraphicView
	{
		id: q301989889
		objId: 301989889
		x: 0
		y: 0
		width: 80
		height: 45
		qm_Transparent : true 
		qm_ImageWidth: 80
		qm_ImageHeight: 45
		qm_SourceSizeWidth: 80
		qm_SourceSizeHeight: 45
	}
}
